import Vue from 'vue'
import Vuex from 'vuex'
import api from '../services/api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todos: []
  },
  actions: {
    addList: ({ commit }, todo) => commit('add_list', todo),
    allList: ({ commit }) => commit('all_list'),
    removeItem: ({ commit }, id) => commit('remove_item', id)
  },
  mutations: {
    add_list: async (state, todo) => {
      state.todos.push(todo)
      await api.post('/posts', todo)
    },
    all_list: async (state) => {
      const response = await api.get('/posts')
      state.todos = response.data
    },
    remove_item: async (state, id) => {
      await api.delete(`/posts/${id}`)
      state.todos = state.todos.filter(resp => resp.id !== id)
    }
  },
  getters: {
    todos: state => state.todos
  },
  modules: {
  }
})
